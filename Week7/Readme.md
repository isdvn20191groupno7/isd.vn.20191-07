# ISD.VN.20191-07 - Homework Week 7

# Work assignment in Architecture Design AFC

* Leader: Nguyễn Đình Minh

## Đỗ Thúy Nga

### Vẽ Er-Diagrams cho:

* Ticket-Card Information
* Ticket

### SDD DataModeling cho:

* Ticket-Card Information
* Ticket

### Sửa lại class diagrams combine

## Nguyễn Trọng Nghĩa

### Vẽ Er-Diagrams cho:

* Trip

### SDD DataModeling cho:

* Trip

### Sửa lại class diagrams combine

## Vũ Đức Nguyễn

### Vẽ Er-Diagrams cho:

* Station
* Prepaid-card

### SDD DataModeling cho:

* Station
* Prepaid-card

### Sửa lại class diagrams combine

## Nguyễn Đình Minh

### Sửa lại class diagrams

### Sửa lại Er-Diagrams của cả nhóm

### Tổng hợp bài tập tuần vào folder #TeamCombine (SDD DataModeling)
