package junitTest.businessLayer.ticket;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import businessLayer.ticket.OnewayTicketBUS;
import dataLayer.OnewayTicketDAO;
import dataTransferObject.OnewayTicket;

public class OnewayTicketBUSTest {

	@Test
	public void testGetTicketById() throws ClassNotFoundException, SQLException {
		String id="OW201909301234";
		assertNotEquals(OnewayTicketDAO.findbyId(id), null);
		
	}

	@Test
	public void testGetTicketByCode() throws ClassNotFoundException, SQLException {
		String code="e8dc4081b13434b4";
		assertNotEquals(OnewayTicketDAO.getByCode(code), null);
	}

	@Test
	public void testCheckEmbarkation() throws ClassNotFoundException, SQLException {
		OnewayTicket owTicket=OnewayTicketDAO.findbyId("OW201909301234");
		int x=OnewayTicketBUS.checkEmbarkation(5,owTicket);
		assertNotEquals(x,0);
	}

	@Test
	public void testCheckDisembarkation() throws ClassNotFoundException, SQLException {
		OnewayTicket owTicket=OnewayTicketDAO.findbyId("OW201909301234");
		int x=OnewayTicketBUS.checkDisembarkation(7,owTicket);
		assertNotEquals(x,0);
	}

	@Test
	public void testCaculateOnewayTicket() throws ClassNotFoundException, SQLException {
		OnewayTicket owTicket=OnewayTicketDAO.findbyId("OW201909301234");
		Double price=OnewayTicketBUS.caculate(5, 7, owTicket);
		assertNotEquals(price, 0);
	}

	@Test
	public void testDisplayOnewayTicketInfo() {
		fail("Not yet implemented");
	}

	@Test
	public void testHandleOnewayTicketWhenEnterStation() {
		fail("Not yet implemented");
	}

	@Test
	public void testHandleOnewayTicketWhenExitStation() {
		fail("Not yet implemented");
	}

}
