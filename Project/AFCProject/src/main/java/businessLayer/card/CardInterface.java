package businessLayer.card;
import java.sql.SQLException;

import dataTransferObject.Card;

/**
 * @author Vu Duc Nguyen
 *
 */
public interface CardInterface {
	/**
	 * @param id
	 * @return
	 */
	public double getCardBalance(String id);	
	/**
	 * @param id
	 * @return
	 */
	public boolean checkBalanceFirst(String id);	
	/**
	 * @param id
	 * @return
	 */
	public boolean checkBalanceWhenExit (String id);	
	/**
	 * @param idStation1
	 * @param idStation2
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public double calculateFee(int idStation1, int idStation2) throws ClassNotFoundException, SQLException;	
	/**
	 * @param card
	 */
	public void displayCardInfor (Card card);
}
