package presentationLayer;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;

/**
 * @author Do Thuy Nga
 *
 */
public class TicketRecognizerInterface {
	public static String process(String pseudoBarCode) throws InvalidIDException {
		TicketRecognizer ticketRecognizer = TicketRecognizer.getInstance();
		return ticketRecognizer.process(pseudoBarCode);
	}
	
	/*public static void main(String[] args) throws InvalidIDException {
//		String pseudoBarCode = "ijklmnop"; // 24h
//		String pseudoBarCode = "abcdefgh"; // ow
		String pseudoBarCode = "ponmlkji"; //24h
	    TicketRecognizer ticketRecognizer = TicketRecognizer.getInstance();
	    String ticketId = ticketRecognizer.process(pseudoBarCode);
//	    System.out.println(ticketId);
	}*/
}
