package dataLayer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dataTransferObject.Ticket;

/**
 * @author Do Thuy Nga
 *
 */
public class TicketDAO {
	public static ArrayList<Ticket> getAllStations() throws ClassNotFoundException, SQLException {
		ArrayList<Ticket> allTicket = new ArrayList<Ticket>();
		String sql = "select * from ticket";
		
		Connection connection = ConnectionUtils.getMyConnection();
		
		Statement statement = connection.createStatement();
		
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
        	Ticket ticket = new Ticket();
        	ticket.setId(rs.getString("id"));
        	ticket.setType(rs.getInt("type"));
        	ticket.setCode(rs.getString("code"));
        	allTicket.add(ticket);
        }
		connection.close();
		return allTicket;
	}
	/**
	 * @param code
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static Ticket findbyCode(String code) throws ClassNotFoundException, SQLException {
		Ticket ticket = new Ticket();
		String sql = "SELECT * from ticket WHERE code="+"\""+code +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
			ticket.setId(rs.getString("id"));
			ticket.setType(rs.getInt("type"));
			ticket.setCode(rs.getString("code"));
        }
		connection.close();
		return ticket;
	}
}
