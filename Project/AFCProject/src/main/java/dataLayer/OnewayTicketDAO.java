package dataLayer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dataTransferObject.OnewayTicket;

/**
 * @author Do Thuy Nga
 *
 */
public class OnewayTicketDAO {
	/**
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static ArrayList<OnewayTicket> onewayTicketAll() throws ClassNotFoundException, SQLException{
		ArrayList<OnewayTicket> arr = new ArrayList<OnewayTicket>();
        String sql = "select * from onewayticket";
     // Láº¥y ra Ä‘á»‘i tÆ°á»£ng Connection káº¿t ná»‘i vÃ o DB.
        Connection connection = ConnectionUtils.getMyConnection();
   
        // Táº¡o Ä‘á»‘i tÆ°á»£ng Statement.
        Statement statement = connection.createStatement();
      
        ResultSet rs = statement.executeQuery(sql);
     // Duyá»‡t trÃªn káº¿t quáº£ tráº£ vá»�.
        while (rs.next()) {// Di chuyá»ƒn con trá»� xuá»‘ng báº£n ghi káº¿ tiáº¿p.
        	
        	OnewayTicket oneway = new OnewayTicket();
            oneway.setId(rs.getString("Id"));
            oneway.setStatus(rs.getInt("status"));
            oneway.setPrice(rs.getFloat("price"));
            oneway.setEmbarkation(rs.getInt("embarkation"));
            oneway.setDisembarkation(rs.getInt("disembarkation"));
            arr.add(oneway);
      
        }
        // Ä�Ã³ng káº¿t ná»‘i
        connection.close();
        return arr;
	}
	
	/**
	 * @param idOneway
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static OnewayTicket findbyId(String idOneway) throws ClassNotFoundException, SQLException {
		OnewayTicket owTicket = new OnewayTicket();
		String sql = "SELECT * from onewayticket WHERE Id="+"\""+idOneway +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
			owTicket.setId(rs.getString("Id"));
			owTicket.setStatus(rs.getInt("status"));
			owTicket.setPrice(rs.getFloat("price"));
			owTicket.setEmbarkation(rs.getInt("embarkation"));
			owTicket.setDisembarkation(rs.getInt("disembarkation"));
			owTicket.setCode(rs.getString("Code"));
        }
		connection.close();
		return owTicket;
	}

	/**
	 * @param code
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static OnewayTicket getByCode(String code) throws ClassNotFoundException, SQLException {
		OnewayTicket owTicket = new OnewayTicket();
		String sql = "SELECT * from onewayticket WHERE Code="+"\""+code +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
			owTicket.setId(rs.getString("Id"));
			owTicket.setStatus(rs.getInt("status"));
			owTicket.setPrice(rs.getFloat("price"));
			owTicket.setEmbarkation(rs.getInt("embarkation"));
			owTicket.setDisembarkation(rs.getInt("disembarkation"));
			owTicket.setCode(rs.getString("Code"));
			}
		connection.close();
		return owTicket;
	}
	
	/**
	 * @param id
	 * @param status
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static boolean updateStatus(String id, int status) throws ClassNotFoundException, SQLException {
		String sql = "UPDATE onewayticket set status="+status+" WHERE Id="+"\""+id +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		boolean check = statement.execute(sql);
		return check;
	}
	
	public static boolean updatePrice(String id, double price) throws ClassNotFoundException, SQLException {
		String sql = "UPDATE onewayticket set price="+price+" WHERE Id="+"\""+id +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		boolean check = statement.execute(sql);
		return check;
	}
	
//	public static void main(String argvs[]) throws ClassNotFoundException, SQLException {
//		updateStatus("OW201909301234", 0);
//	}
}
