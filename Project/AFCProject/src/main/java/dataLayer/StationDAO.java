package dataLayer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import dataTransferObject.Station;

/**
 * @author Do Thuy Nga
 *
 */
public class StationDAO {
	/**
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static ArrayList<Station> getAllStations() throws ClassNotFoundException, SQLException {
		ArrayList<Station> allStations = new ArrayList<Station>();
		String sql = "select * from station";
		
		Connection connection = ConnectionUtils.getMyConnection();
		
		Statement statement = connection.createStatement();
		
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {// Di chuyá»ƒn con trá»� xuá»‘ng báº£n ghi káº¿ tiáº¿p.
        	Station station = new Station();
        	station.setStationID(rs.getInt("stationID"));
        	station.setStationName(rs.getString("stationName"));
        	station.setDistance(rs.getDouble("distance"));
        	allStations.add(station);
        }
		connection.close();
		return allStations;
	}
	
	/**
	 * @param id
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static Station getStationById(Integer id) throws ClassNotFoundException, SQLException {
		Station station = new Station();
		
		String sql = "SELECT * from station WHERE StationID="+id;
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
			station.setStationID(rs.getInt("stationID"));
        	station.setStationName(rs.getString("stationName"));
        	station.setDistance(rs.getDouble("distance"));
        }
		connection.close();
		return station;
	}
	
	/**
	 * @param id
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static Double getDistanceById(Integer id) throws ClassNotFoundException, SQLException {
		Station station = new Station();
		
		String sql = "SELECT * from station WHERE StationID="+id;
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
        	station.setDistance(rs.getDouble("distance"));
        }
		connection.close();
		return station.getDistance();
	}
	
	/**
	 * @param id
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static String getStationNameById(Integer id) throws ClassNotFoundException, SQLException {
		Station station = new Station();
		
		String sql = "SELECT * from station WHERE StationID="+id;
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
        	station.setStationName(rs.getString("stationName"));
        }
		connection.close();
		return station.getStationName();
	}
	
//	public static void main(String argv[]) throws ClassNotFoundException, SQLException {
//		ArrayList<Station> allStations = new ArrayList<Station>();
//		allStations = getAllStations();
//		allStations.size();
//		System.out.println(allStations.size());
//		Station station = getStationById(3);
//		System.out.println(station.getStationName()+station.getDistance()+station.getStationID());
//		System.out.println(getDistanceById(4));
//	}
}
