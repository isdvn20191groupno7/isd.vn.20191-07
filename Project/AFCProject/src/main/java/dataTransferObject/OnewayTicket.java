package dataTransferObject;

/**
 * @author Do Thuy Nga
 *
 */
public class OnewayTicket extends Ticket {
	private int status;
	private int embarkation;
	private int disembarkation;
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getEmbarkation() {
		return embarkation;
	}
	public void setEmbarkation(int embarkation) {
		this.embarkation = embarkation;
	}
	public int getDisembarkation() {
		return disembarkation;
	}
	public void setDisembarkation(int disembarkation) {
		 this.disembarkation = disembarkation;
	}
	
}
