# ISD.VN.20191-07 - Automated Fare Controller

# Work assignment in Homework Week 10

* Leader: Nguyễn Đình Minh

## Đỗ Thúy Nga

### Tuân thủ High Cohesion and Loose Coupling và viết lại class:

* One-way Ticket class

## Nguyễn Trọng Nghĩa

### Tuân thủ High Cohesion and Loose Coupling và viết lại class:

* Station class

## Vũ Đức Nguyễn

### Tuân thủ High Cohesion and Loose Coupling và viết lại class:

* Prepaid-card class

## Nguyễn Đình Minh

### Tuân thủ High Cohesion and Loose Coupling và viết lại class:

* Twenty-four Hours Ticket class

## Đỗ Quốc Nam

### Tuân thủ High Cohesion and Loose Coupling và viết lại class:

* Gate Inteface class
