package test.businessLayer;

import static org.junit.Assert.*;

import businessLayer.PrepaidCardBUS;
import org.junit.Before;
import org.junit.Test;

public class PrepaidCardBUSTest {
	
	private PrepaidCardBUS prepaidCardBUSTest;
	@Before
	public void setUp() throws Exception {
		prepaidCardBUSTest = new PrepaidCardBUS();
	}

	@Test
	public void testGetCardBalance() {
		double balance = PrepaidCardBUS.getCardBalance("2345");
		double result = 25;
		assertEquals(result, balance, 0);
	}

	@Test
	public void testCheckBalanceFirst() {
		boolean check1 = prepaidCardBUSTest.checkBalanceFirst("2345");
		assertEquals(true, check1);
		
		boolean check2 = prepaidCardBUSTest.checkBalanceFirst("4444");
		assertEquals(false, check2);
	}

	@Test
	public void testUpdateBalance() {
		double balance = prepaidCardBUSTest.updateBalance("4444", 2.5);
		double result = -1;
		assertEquals(result, balance, 0);
	}

	@Test
	public void testCheckBalanceWhenExit() {
		boolean check1 = prepaidCardBUSTest.checkBalanceFirst("2345");
		assertEquals(true, check1);
		
		boolean check2 = prepaidCardBUSTest.checkBalanceFirst("4444");
		assertEquals(false, check2);
	}

	@Test
	public void testDisplayCardInfor() {
		String display = prepaidCardBUSTest.displayCardInfor("4444");
		assertEquals("id: 4444, Balance: 1.5", "id: 4444, Balance: 1.5");
		String display2 = prepaidCardBUSTest.displayCardInfor("0000");
		assertEquals("Không tìm thấy card", "Không tìm thấy card");
	}

}
