package beforeThreeLayer.testSDK.nguyendinhminh;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.scanner.CardScanner;

public class CardScannerSDK {
    public static void main(String[] args) throws InvalidIDException {
        String barCode = "ABCDEFGH";
        CardScanner scanner = CardScanner.getInstance();
        String cardID = scanner.process(barCode);
        System.out.println(cardID);
    }
}
