package dataLayer.twentyfourHourTicket;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dataLayer.ConnectionUtils;
import dataTransferObject.TwentyHoursTicket;

public class TwentyfourHourTicketDAO {
	public static ArrayList<TwentyHoursTicket> twentyfourHoursTicketList() throws ClassNotFoundException, SQLException{
		ArrayList<TwentyHoursTicket> arr = new ArrayList<TwentyHoursTicket>();
        String sql = "select * from twentyfourhours-ticket";
        Connection connection = ConnectionUtils.getMyConnection();
   
        Statement statement = connection.createStatement();
      
        ResultSet rs = statement.executeQuery(sql);
        while (rs.next()) {
        	
        	TwentyHoursTicket tfhTicket = new TwentyHoursTicket();
        	tfhTicket.setId(rs.getInt("Id"));
        	tfhTicket.setStatus(rs.getString("status"));
        	tfhTicket.setPrice(rs.getFloat("price"));
            arr.add(tfhTicket);
      
        }
        connection.close();
        return arr;
	}
	
	public static TwentyHoursTicket findbyId(int idOneway) throws ClassNotFoundException, SQLException {
		ArrayList<TwentyHoursTicket> arr=twentyfourHoursTicketList();
		for(TwentyHoursTicket tfhTicket: arr) {
			if(tfhTicket.getId()==idOneway)
				return tfhTicket;
		}
		return null;
	}
}