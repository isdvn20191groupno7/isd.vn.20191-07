package businessLayer;

import java.sql.SQLException;

import dataLayer.PrepaidCardDAO;
import dataTransferObject.PrepaidCard;

public class PrepaidCardBUS {
	
	public static double getCardBalance(String id) {
		PrepaidCard card = null;
		try {
			card = PrepaidCardDAO.getCard(id);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return card.getBalance();
	}
	
	public static boolean checkBalanceFirst(String id) {
		double balance = getCardBalance(id);
		
		if (balance > 1.9) {
			return true;
		} else {
			return false;
		}
	}
	
	public static double calculateFee(int idStation1, int idStation2) {
		// Todo
		return -1;
	}
	
	public static double updateBalance(String id, double calculateFee) {
		double balance = getCardBalance(id);
		double updateBalance = balance - calculateFee;
		if(updateBalance < 0) {
			System.out.println("Không thể cập nhật số dư");
			return updateBalance;
		} else {
			try {
				PrepaidCardDAO.updateBalance(id, updateBalance);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return updateBalance;
		}
	}
	
	public static boolean checkBalanceWhenExit (String id) {
		double balance = getCardBalance(id);
		
		if (balance > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public static String displayCardInfor (String id) {
		PrepaidCard card = null;
		try {
			card = PrepaidCardDAO.getCard(id);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(card != null) {
			return "id: " + card.getId() + ", Balance: " + card.getBalance();
		}
		return "Không tìm thấy card";
	}
	
	
//	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
//		boolean check = PrepaidCardDAO.updateBalance("12345", 11.5);
//		PrepaidCardDAO prepaidCard = new PrepaidCardDAO();
//		ArrayList<PrepaidCard> allCards = PrepaidCardDAO.getAllPrepaidCard();
//		for (PrepaidCard card : allCards) {
//			System.out.println(card.getId() + " " + card.getBalance());
//		}
		
//		PrepaidCardBUS check = new PrepaidCardBUS();
//		checkBus.getCardBalance("12345");
//		System.out.println(checkBus.getCardBalance("123456"));
		
//		PrepaidCard card = PrepaidCardDAO.getCard("123456");
//		System.out.println(card.getBalance() + "haha");
//		
//		System.out.println(checkBalanceFirst("4444"));
		
//		System.out.println(displayCardInfor("2222"));
//	}

}
