package businessLayer;

public class TicketBUS {
	ITicketBUS iticketBUS;
	
	public TicketBUS(ITicketBUS itk) {
		this.iticketBUS = itk;
	}
	
	public String calType(){
		return iticketBUS.getTicketType();
	}
	
	public String getTicketType() {
		String type;
		String rCTwentyFourTicket = "TF";
		
		String representCharacter= calType();
		
		if (representCharacter.compareTo(rCTwentyFourTicket) == 0) {
			type = "Twenty-four-hour Ticket";
		} else
			type = "One-way Ticket";
		return type;
	}
}