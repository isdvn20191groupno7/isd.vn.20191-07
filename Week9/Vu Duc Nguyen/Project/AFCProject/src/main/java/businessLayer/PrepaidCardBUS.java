package businessLayer;

import java.sql.SQLException;

import dataLayer.PrepaidCardDAO;
import dataLayer.StationDAO;
import dataTransferObject.PrepaidCard;

public class PrepaidCardBUS {
	public static PrepaidCard getCardById( String id) throws ClassNotFoundException, SQLException {
		return PrepaidCardDAO.getCardById(id);
	}
	
	public static PrepaidCard getCardByCode( String code) throws ClassNotFoundException, SQLException {
		return PrepaidCardDAO.getCardByCode(code);
	}
	
	public static double getCardBalance(String id) {
		PrepaidCard card = null;
		try {
			card = PrepaidCardDAO.getCardById(id);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return card.getBalance();
	}
	
	public static boolean checkBalanceFirst(String id) {
		double balance = getCardBalance(id);
		
		if (balance > 1.9) {
			return true;
		} else {
			return false;
		}
	}
	
	public static double calculateFee(int idStation1, int idStation2) throws ClassNotFoundException, SQLException {
		double distance = Math.abs(StationDAO.getDistanceById(idStation1) - StationDAO.getDistanceById(idStation2));
		if( distance <= 5 ) {
			return 1.9;
		}else {
			return 1.9 + Math.ceil((distance-5)/2)*0.4;
		}
	}
	
	public static double updateBalance(String id, double calculateFee) {
		double balance = getCardBalance(id);
		double updateBalance = balance - calculateFee;
		if(updateBalance < 0) {
			System.out.println("Không thể cập nhật số dư");
			return updateBalance;
		} else {
			try {
				PrepaidCardDAO.updateBalance(id, updateBalance);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return updateBalance;
		}
	}
	
	public static boolean checkBalanceWhenExit (String id) {
		double balance = getCardBalance(id);
		
		if (balance > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public static void displayCardInfor (PrepaidCard card) {
		System.out.println("Type: Prepaid Card \t ID: " + card.getId());
		System.out.println("Balance: "+ card.getBalance());
	}
	
//	public static void main(String[] args) throws ClassNotFoundException, SQLException {
//		// TODO Auto-generated method stub
//		
//		PrepaidCard card = PrepaidCardDAO.getCardByCode("9ac2197d9258257b");
//		System.out.println(calculateFee(4,7));
//	}

}
