package dataLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnUtils {
	// Kết nối vào MySQL.
		 public static Connection getMySQLConnection() throws SQLException,ClassNotFoundException {
		     // tự config theo tên
			 String hostName = "localhost";
		     String dbName = "afc_database";
		     String userName = "root";
		     String password = "12345";
		 
		     return getMySQLConnection(hostName, dbName, userName, password);
		 }
		 
		 public static Connection getMySQLConnection(String hostName, String dbName,
		         String userName, String password) throws SQLException,
		         ClassNotFoundException {
		     Class.forName("com.mysql.cj.jdbc.Driver");

		     String connectionURL = "jdbc:mysql://" + hostName + ":3306/" + dbName + "?useSSL=false";
		 
		     Connection conn = DriverManager.getConnection(connectionURL, userName,
		             password);
		     return conn;
		 }
}
