package dataLayer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dataTransferObject.PrepaidCard;
import dataTransferObject.Station;

public class StationDAO {
	public static ArrayList<Station> getAllStations() throws ClassNotFoundException, SQLException {
		ArrayList<Station> allStations = new ArrayList<Station>();
		String sql = "select * from station";
		
		Connection connection = ConnectionUtils.getMyConnection();
		
		Statement statement = connection.createStatement();
		
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {// Di chuyển con trỏ xuống bản ghi kế tiếp.
        	Station station = new Station();
        	station.setStationID(rs.getInt("stationID"));
        	station.setStationName(rs.getString("stationName"));
        	station.setDistance(rs.getDouble("distance"));
        	allStations.add(station);
        }
		connection.close();
		return allStations;
	}
	
	public static Station getStationById(Integer id) throws ClassNotFoundException, SQLException {
		Station station = new Station();
		
		String sql = "SELECT * from station WHERE StationID="+id;
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
			station.setStationID(rs.getInt("stationID"));
        	station.setStationName(rs.getString("stationName"));
        	station.setDistance(rs.getDouble("distance"));
        }
		connection.close();
		return station;
	}
	
	public static Double getDistanceById(Integer id) throws ClassNotFoundException, SQLException {
		Station station = new Station();
		
		String sql = "SELECT * from station WHERE StationID="+id;
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
        	station.setDistance(rs.getDouble("distance"));
        }
		connection.close();
		return station.getDistance();
	}
	
	public static void main(String argv[]) throws ClassNotFoundException, SQLException {
		ArrayList<Station> allStations = new ArrayList<Station>();
		allStations = getAllStations();
		allStations.size();
		System.out.println(allStations.size());
		Station station = getStationById(3);
		System.out.println(station.getStationName()+station.getDistance()+station.getStationID());
		System.out.println(getDistanceById(4));
	}
}
